db.users.insertMany([
    {
	    "firstName": "Sangonomiya",
		"lastName": "Kokomi",
		"email": "iWantToSleep@gmail.com",
		"password": "fish234",
		"isAdmin": false				
    },
    {
	    "firstName": "Kujou",
		"lastName": "Sara",
		"email": "forTheShogun@gmail.com",
		"password": "tooSerious",
		"isAdmin": false				
    },
        {
	    "firstName": "Barbara",
		"lastName": "Pegg",
		"email": "idolBarbruh@gmail.com",
		"password": "Ikuyoooo",
		"isAdmin": false				
    },
        {
	    "firstName": "Eula",
		"lastName": "Lawrence",
		"email": "vengeanceWillBeMine@gmail.com",
		"password": "transgressions",
		"isAdmin": false				
    },
        {
	    "firstName": "Raiden",
		"lastName": "Shogun",
		"email": "kagemusha@gmail.com",
		"password": "BoobaSword",
		"isAdmin": false				
    }    
])

db.courses.insertMany([
	{
		"name": "RPG 101", 
		"description": "Basics of RPGs",
		"price": 3000,
		"isActive": true	
	},
	{
		"name": "RPG 103",
		"description": "Online gaming proper conduct",
		"price": 2000, 
		"isActive": true	
	},
	{
		"name":  "RPG 131",
		"description": "Team Compositions",
		"price": 4500,
		"isActive": true	
	},
	
])

db.users.updateOne({"firstName":"Raiden"},{$set:{"isAdmin":true}});

db.users.find({"isAdmin":false});

db.courses.updateOne({"price": 2000},{$set:{"isActive": false}});